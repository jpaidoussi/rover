# Mar Rover Challenge

## Requirements

This project requires Python 3.6 and [pipenv](https://docs.pipenv.org/#install-pipenv-today)

## Installation

To install, run:

`pipenv install`

To run the tests, run:

`pipenv run py.test`

To run the rover program, run:

`pipenv run python rover.py <file with commands>`

e.g.:

`pipenv run python rover.py test_command.txt`


## Decisions and Assumptions

The rover's intended behaviour when reaching a zone boundary was not specified so the assumption I made is that it is not safe for a rover to move outside of its zone. Therefore individual movement commands that would take the rover outside its zone are ignored.

The core components of the program (with the exception of the Rover itself) are built using immutable data structures from the [attrs](http://www.attrs.org/) project. This decision was made to ensure the only place that contained mutable state was the rover itself. The other components are therefore free from unintended side effects changing state.

The testing framework for this project tests each of the components individually and does a full integration test on the Rover as well.