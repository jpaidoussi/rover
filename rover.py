import attr
import click
import re

from collections import OrderedDict


VALID_MOVEMENTS = re.compile(r'^[MLR]*$')
BEARINGS = OrderedDict([
    ('N', (0, 1)),
    ('E', (1, 0)),
    ('S', (0, -1)),
    ('W', (-1, 0))
])


@attr.s(frozen=True)
class Position:
    x: int = attr.ib(converter=int)
    y: int = attr.ib(converter=int)

    def move(self, x, y):
        """Move by the given amount in each direction.

        Returns a new Position object.
        """
        return self.__class__(x=self.x + x, y=self.y + y)


@attr.s(frozen=True)
class Zone:
    x_boundary: int = attr.ib(converter=int)
    y_boundary: int = attr.ib(converter=int)

    def is_valid_position(self, position):
        if position.x >= self.x_boundary or position.x < 0:
            return False
        if position.y >= self.y_boundary or position.y < 0:
            return False

        return True


@attr.s(frozen=True)
class Bearing:
    direction = attr.ib()

    @direction.validator
    def _check_direction(self, attribute, value):
        if value not in ('N', 'E', 'S', 'W'):
            raise ValueError("Direction can only be one of 'N', 'E', 'S', 'W'")

    def turn_left(self):
        """If we Assume the BEARINGS keys are circular list, turning left is finding
        the index of element on the left of the current bearing.

        Returns a new Bearing object.
        """
        keys = list(BEARINGS.keys())
        index = (keys.index(self.direction) - 1) % len(keys)
        return self.__class__(keys[index])

    def turn_right(self):
        """IF we assume the BEARINGS keys are circular list, turning right is finding
        the index of element on the right of the current bearing.

        Returns a new Bearing object.
        """
        keys = list(BEARINGS.keys())
        index = (keys.index(self.direction) + 1) % len(keys)
        return self.__class__(keys[index])

    @property
    def movement(self):
        return BEARINGS[self.direction]


@attr.s(frozen=True)
class RoverCommand:
    zone = attr.ib()
    position = attr.ib()
    bearing = attr.ib()
    movements = attr.ib()

    @movements.validator
    def _validate_movements(self, attribute, value):
        if not VALID_MOVEMENTS.match(value):
            raise ValueError("Movements must only be one of 'M', 'L', 'R'")

    @classmethod
    def from_line_input(cls, input):
        lines = input.splitlines()
        if len(lines) < 3:
            raise ValueError("Commands must be supplied as three lines")
        zone = lines[0]
        if len(zone) != 2:
            raise ValueError("Zone must be supplied as two integers, e.g. '99'")
        try:
            position, direction = lines[1].split(' ')
        except ValueError:
            raise ValueError("Position and direction must be separated by a space")
        if len(position) != 2:
            raise ValueError("Position must be supplied as two integers, e.g. '12'")
        return cls(
            zone=Zone(x_boundary=zone[0], y_boundary=zone[1]),
            position=Position(x=position[0], y=position[1]),
            bearing=Bearing(direction=direction),
            movements=lines[2]
        )


@attr.s
class Rover:
    position = attr.ib(default=None)
    bearing = attr.ib(default=None)
    zone = attr.ib(default=None)

    def move_forward(self):
        if not self.position or not self.zone or not self.bearing:
            raise ValueError("Position, bearing and zone must be set first")

        new_position = self.position.move(*self.bearing.movement)
        # Assumption: the rover should not move outside of its zone,
        # so ignore movement commands that would take it outside.
        if self.zone.is_valid_position(new_position):
            self.position = new_position

    def turn_left(self):
        if not self.bearing:
            raise ValueError("Bearing must be set first")
        self.bearing = self.bearing.turn_left()

    def turn_right(self):
        if not self.bearing:
            raise ValueError("Bearing must be set first")
        self.bearing = self.bearing.turn_right()

    def perform_command(self, command):
        self.position = command.position
        self.bearing = command.bearing
        self.zone = command.zone
        for movement in command.movements:
            if movement == 'M':
                self.move_forward()
            elif movement == 'L':
                self.turn_left()
            elif movement == 'R':
                self.turn_right()


@click.command()
@click.argument('command_file', type=click.Path(exists=True))
def send(command_file):
    with open(command_file) as fp:
        commands = fp.read().strip()
        try:
            rc = RoverCommand.from_line_input(commands)
        except ValueError as excinfo:
            error = str(excinfo)
            click.echo(f"Invalid command file: {error}")
            return

        rover = Rover()
        rover.perform_command(rc)
        click.echo(f"{rover.position.x}{rover.position.y} {rover.bearing.direction}")


if __name__ == '__main__':
    send()
