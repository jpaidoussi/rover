import pytest
import rover


class TestPosition:
    @pytest.mark.parametrize('test_x,test_y,expected', [
        (8, 8, (8, 8)),
        ('8', '8', (8, 8)),
        ('1', 8, (1, 8)),
        (2, '2', (2, 2)),
    ])
    def test_creation(self, test_x, test_y, expected):
        pos = rover.Position(x=test_x, y=test_y)
        assert pos.x == expected[0]
        assert pos.y == expected[1]

    @pytest.mark.parametrize('test_position,test_move,expected', [
        ((8, 8), (1, 0), (9, 8)),
        ((8, 8), (0, 1), (8, 9)),
        ((8, 8), (1, 1), (9, 9)),
    ])
    def test_move(self, test_position, test_move, expected):
        pos = rover.Position(x=test_position[0], y=test_position[1])
        new_pos = pos.move(*test_move)
        assert new_pos.x == expected[0]
        assert new_pos.y == expected[1]


class TestZone:
    @pytest.mark.parametrize('test_zone,test_pos,valid', [
        ((8, 8), (0, 0), True),
        ((8, 8), (0, 0), True),
        ((8, 8), (8, 8), False),
        ((8, 8), (8, 7), False),
        ((8, 8), (7, 8), False),
        ((8, 8), (7, 7), True),
        ((8, 8), (-1, 0), False),
        ((8, 8), (-1, -1), False),
        ((8, 8), (0, -1), False),
    ])
    def test_boundaries(self, test_zone, test_pos, valid):
        zone = rover.Zone(*test_zone)
        position = rover.Position(*test_pos)
        assert zone.is_valid_position(position) == valid


class TestBearing:
    @pytest.mark.parametrize('test_input,exp_direction,exp_movement', [
        ('N', 'N', (0, 1)),
        ('E', 'E', (1, 0)),
        ('S', 'S', (0, -1)),
        ('W', 'W', (-1, 0)),
    ])
    def test_creation(self, test_input, exp_direction, exp_movement):
        bearing = rover.Bearing(direction=test_input)
        assert bearing.direction == exp_direction
        assert bearing.movement == exp_movement

    @pytest.mark.parametrize('test_input', [
        'n',
        0,
        1,
        'a',
        'NNNEWWW',
        'NN',
        'NW',
    ])
    def test_invalid_creation(self, test_input):
        with pytest.raises(ValueError):
            rover.Bearing(test_input)

    @pytest.mark.parametrize('test_input,expected', [
        ('N', 'W'),
        ('E', 'N'),
        ('S', 'E'),
        ('W', 'S'),
    ])
    def test_turn_left(self, test_input, expected):
        bearing = rover.Bearing(direction=test_input)
        assert bearing.turn_left().direction == expected

    @pytest.mark.parametrize('test_input,expected', [
        ('N', 'E'),
        ('E', 'S'),
        ('S', 'W'),
        ('W', 'N'),
    ])
    def test_turn_right(self, test_input, expected):
        bearing = rover.Bearing(direction=test_input)
        assert bearing.turn_right().direction == expected


class TestRoverCommand:
    @pytest.mark.parametrize('test_input,expected', [
        ("88\n12 E\nMMLMRMMRRMML", ((8, 8), (1, 2), 'E', 'MMLMRMMRRMML')),
        ("44\n22 N\nRLRLMMLL", ((4, 4), (2, 2), 'N', 'RLRLMMLL')),
    ])
    def test_valid_from_line_input(self, test_input, expected):
        rc = rover.RoverCommand.from_line_input(test_input)
        assert rc.zone.x_boundary == expected[0][0]
        assert rc.zone.y_boundary == expected[0][1]
        assert rc.position.x == expected[1][0]
        assert rc.position.y == expected[1][1]
        assert rc.bearing.direction == expected[2]
        assert rc.movements == expected[3]

    @pytest.mark.parametrize('test_input,expected', [
        ("88\n12 E", "Commands must be supplied as three lines"),
        ("88", "Commands must be supplied as three lines"),
        ("8\n12 E\nMMLMRMMRRMML", "Zone must be supplied as two integers, e.g. '99'"),
        ("88\n2 E\nMMLMRMMRRMML", "Position must be supplied as two integers, e.g. '12'"),
        ("88\n12E\nMMLMRMMRRMML", "Position and direction must be separated by a space"),
        ("88\n12 A\nMMLMRMMRRMML", "Direction can only be one of 'N', 'E', 'S', 'W'"),
        ("88\n12 E\nMMLRA", "Movements must only be one of 'M', 'L', 'R'"),
    ])
    def test_invalid_from_line_input(self, test_input, expected):
        with pytest.raises(ValueError) as excinfo:
            rover.RoverCommand.from_line_input(test_input)
        assert expected in str(excinfo.value)


class TestRover:
    @pytest.mark.parametrize('test_input,expected', [
        ('W', 'S'),
        ('S', 'E'),
        ('E', 'N'),
        ('N', 'W'),
    ])
    def test_turn_left(self, test_input, expected):
        test_rover = rover.Rover(
            position=rover.Position(x=1, y=1),
            bearing=rover.Bearing(test_input),
            zone=rover.Zone(x_boundary=8, y_boundary=8)
        )
        test_rover.turn_left()
        assert test_rover.bearing.direction == expected

    @pytest.mark.parametrize('test_input,expected', [
        ('W', 'N'),
        ('S', 'W'),
        ('E', 'S'),
        ('N', 'E'),
    ])
    def test_turn_right(self, test_input, expected):
        test_rover = rover.Rover(
            position=rover.Position(x=1, y=1),
            bearing=rover.Bearing(test_input),
            zone=rover.Zone(x_boundary=8, y_boundary=8)
        )
        test_rover.turn_right()
        assert test_rover.bearing.direction == expected

    @pytest.mark.parametrize('pos,bear,zone,expected', [
        ((2, 2), 'N', (4, 4), (2, 3, 'N')),
        ((2, 2), 'E', (4, 4), (3, 2, 'E')),
        ((2, 2), 'S', (4, 4), (2, 1, 'S')),
        ((2, 2), 'W', (4, 4), (1, 2, 'W')),
    ])
    def test_move_forward_in_zone(self, pos, bear, zone, expected):
        test_rover = rover.Rover(
            position=rover.Position(x=pos[0], y=pos[1]),
            bearing=rover.Bearing(bear),
            zone=rover.Zone(x_boundary=zone[0], y_boundary=zone[1])
        )
        test_rover.move_forward()
        assert test_rover.position.x == expected[0]
        assert test_rover.position.y == expected[1]
        assert test_rover.bearing.direction == expected[2]

    @pytest.mark.parametrize('pos,bear,zone,expected', [
        ((3, 4), 'N', (4, 4), (3, 4, 'N')),
        ((4, 3), 'E', (4, 4), (4, 3, 'E')),
        ((1, 0), 'S', (4, 4), (1, 0, 'S')),
        ((0, 1), 'W', (4, 4), (0, 1, 'W')),
    ])
    def test_move_forward_at_zone_edge(self, pos, bear, zone, expected):
        test_rover = rover.Rover(
            position=rover.Position(x=pos[0], y=pos[1]),
            bearing=rover.Bearing(bear),
            zone=rover.Zone(x_boundary=zone[0], y_boundary=zone[1])
        )
        test_rover.move_forward()
        assert test_rover.position.x == expected[0]
        assert test_rover.position.y == expected[1]
        assert test_rover.bearing.direction == expected[2]

    @pytest.mark.parametrize('command,expected', [
        (rover.RoverCommand.from_line_input('88\n12 E\nMMLMRMMRRMML'), (3, 3, 'S')),
        (rover.RoverCommand.from_line_input('88\n12 E\nRMMMLMMMR'), (4, 0, 'S')),
        (rover.RoverCommand.from_line_input('88\n12 E\nMMMLLMMM'), (1, 2, 'W')),
    ])
    def test_perform_command(self, command, expected):
        test_rover = rover.Rover()
        test_rover.perform_command(command)
        assert test_rover.position.x == expected[0]
        assert test_rover.position.y == expected[1]
        assert test_rover.bearing.direction == expected[2]
